import React, { Component } from "react";
import { connect } from "react-redux";
class ProductItem extends Component {
  setSelectedProduct = (prod) => {
    this.props.dispatch({
      type: "SELECTED_PRODUCT",
      payload: prod,
    });
  };
  addToCart = (prod) => {
    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: prod,
    });
  };
  render() {
    const { name, img } = this.props.prod;
    return (
      <div className="card">
        <img style={{ height: 250, width: "100%" }} src={img} alt="product" />
        <div className="p-2">
          <h4>{name}</h4>
          <button
            onClick={() => {
              this.setSelectedProduct(this.props.prod);
            }}
            className="btn btn-info  "
          >
            Chi Tiết
          </button>
          <button
            onClick={() => {
              this.addToCart(this.props.prod);
            }}
            className="btn btn-danger"
          >
            Thêm Giỏ Hàng
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(ProductItem);
