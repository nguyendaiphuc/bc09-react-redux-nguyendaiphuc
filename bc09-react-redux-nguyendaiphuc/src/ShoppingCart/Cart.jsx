import React, { Component } from "react";
import { connect } from "react-redux";
class Cart extends Component {
  decreaseQuantity = (id) => {
    this.props.dispatch({
      type: "DECREASE_QUANTITY",
      payload: id,
    });
  };
  increaseQuantity = (id) => {
    this.props.dispatch({
      type: "INCREASE_QUANTITY",
      payload: id,
    });
  };
  deleteProduct = (id) => {
    this.props.dispatch({
      type: "DELETE_PRODUCT",
      payload: id,
    });
  };
  renderCart = () => {
    return this.props.cart.map((item) => {
      const { id, img, name, price } = item.product;
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>
            <img style={{ width: 120 }} src={img} alt="product" />
          </td>
          <td>{name}</td>
          <td>
            <button
              onClick={() => {
                this.decreaseQuantity(id);
              }}
              className="btn btn-info"
            >
              -
            </button>
            <span>{item.quantity}</span>
            <button
              onClick={() => {
                this.increaseQuantity(id);
              }}
              className="btn btn-info"
            >
              +
            </button>
          </td>
          <td>{price}</td>
          <td>{item.quantity * price}</td>
          <td>
            <button
              onClick={() => {
                this.deleteProduct(id);
              }}
              className="btn btn-danger"
            >
              {" "}
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Giỏ Hàng</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>Mã Sp</th>
                    <th>Hình Ảnh</th>
                    <th>Tên</th>
                    <th>Số Lượng</th>
                    <th>Đơn Giá</th>
                    <th>Thành Tiền</th>
                  </tr>
                </thead>
                <tbody>{this.renderCart()}</tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                onClick={this.props.makePayment}
                type="button"
                className="btn btn-primary"
              >
                Thanh Toán
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cart: state.cart.cart,
  };
};
export default connect(mapStateToProps)(Cart);
