import React, { Component } from "react";
import Cart from "./Cart";
import Detail from "./Detail";
import ProductList from "./ProductList";
import { connect } from "react-redux";
class Home extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center"> Bài Tập Giỏ Hàng</h1>
        <h4
          data-toggle="modal"
          data-target="#modelId"
          className="text-center  text-danger"
        >
          {" "}
          Cart
        </h4>
        <Cart />
        <ProductList />
        {this.props.selectedProduct && <Detail />}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    selectedProduct: state.product.selectedProduct,
  };
};
export default connect(mapStateToProps)(Home);
