const initialState = {
  cart: [],
};

const reducer = (state = initialState, action) => {
  //Chỉnh sửa state theo action, phân biệt action bằng type
  switch (action.type) {
    case "ADD_TO_CART": {
      const cloneCart = [...state.cart];
      const index = cloneCart.findIndex(
        (item) => item.product.id === action.payload.id
      );
      if (index === -1) {
        const cartItem = { product: action.payload, quantity: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].quantity++;
      }
      state.cart = cloneCart;
      return { ...state };
    }
    case "DECREASE_QUANTITY": {
      const cloneCart = [...state.cart];
      const index = cloneCart.findIndex(
        (item) => item.product.id === action.payload
      );
      cloneCart[index].quantity--;
      state.cart = cloneCart;
      return { ...state };
    }
    case "INCREASE_QUANTITY": {
      const cloneCart = [...state.cart];
      const index = cloneCart.findIndex(
        (item) => item.product.id === action.payload
      );
      cloneCart[index].quantity++;
      state.cart = cloneCart;
      return { ...state };
    }
    case "DELETE_PRODUCT": {
      const cloneCart = [...state.cart];
      const index = cloneCart.findIndex(
        (item) => item.product.id === action.payload
      );
      cloneCart.splice(index, 1);
      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};
export default reducer;
